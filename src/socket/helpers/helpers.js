import * as config from '../config';
import notifications from './notifications';
import storage from './storage';
import { texts } from '../../data';

function prepareForGame({ io, searchRoom, roomName, rooms, roomIdx, updateUsers }) {
  searchRoom.isAvailable = false;
  io.emit(notifications.REMOVE_ROOM, roomName);
  rooms[roomIdx] = { ...searchRoom, users: updateUsers };
  storage.rooms = rooms;
  io.to(roomName).emit(notifications.PREPARE_FOR_GAME, config.SECONDS_TIMER_BEFORE_START_GAME);
  
  const textId = getRandomNumber(0, texts.length - 1);
  const users = {};
  updateUsers.forEach(el => users[el[0]] = { progress: 0, finishTime: 0});
  storage.games.set(roomName, { textId, users });
}

function setGameConfig({ storage, username }) {
  const roomName = storage.activeUsers.get(username).room;
  const id = storage.games.get(roomName).textId;
  return {
    id,
    time: config.SECONDS_FOR_GAME
  }
}

function getRoomName(username) {
  return storage.activeUsers.get(username).room;
}

export function getRandomNumber(min, max) {
  const rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

export {
  prepareForGame,
  setGameConfig,
  getRoomName
};