const storage = {
    activeUsers: new Map(),
    games: new Map(),
    rooms: [],
};

export default storage;