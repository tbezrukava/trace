const notifications = {
    CONNECTION: 'connection',
    DISCONNECT: 'disconnect',
    LOGIN_ERROR: 'login_error',
    ROOM_NAME_ERROR: 'room_name_error',
    CREATE_ROOM: 'create_room',
    ADD_ROOM: 'add_room',
    JOIN_ROOM: 'join_room',
    RENDER_ROOMS: 'render_rooms',
    UPDATE_ROOMS: 'update_rooms',
    LEAVE_ROOM: 'leave_room',
    CHANGE_STATUS: 'change_status',
    REMOVE_ROOM: 'remove_room',
    ADD_USER_TO_ROOM: 'add_user_to_room',
    UPDATE_NUMBER_OF_USERS: 'update_numbers_of_users',
    PREPARE_FOR_GAME: 'prepare_for_game',
    GET_GAME_CONFIG: 'get_game_config',
    SET_GAME_CONFIG: 'set_game_config',
    SET_PROGRESS: 'set_progress',
    UPDATE_PROGRESS: 'update_progress',
    GET_RESULT: 'get_result',
    SET_RESULT: 'set_result'
};

export default notifications;