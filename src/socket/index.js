import { remove } from 'lodash';
import * as config from './config';
import storage from './helpers/storage';
import notifications from './helpers/notifications';
import * as helpers from './helpers/helpers';

export default (io) => {
	io.on(notifications.CONNECTION, socket => {
		const username = socket.handshake.query.username;
		if (storage.activeUsers.has(username)) {
			socket.emit(notifications.LOGIN_ERROR);
			socket.on(notifications.DISCONNECT, () => {})
		} else {
			storage.activeUsers.set(username, { room: null });
			const rooms = storage.rooms.filter(el => el.isAvailable);
			socket.emit(notifications.RENDER_ROOMS, rooms);
		}

		socket.on(notifications.CREATE_ROOM, roomName => {
			const roomAlreadyExist = storage.rooms.find(el => el.name === roomName)
			if (roomAlreadyExist) {
				socket.emit(notifications.ROOM_NAME_ERROR);
			} else {
				socket.join(roomName);
				storage.activeUsers.set(username, { room: roomName });
				const room = { name: roomName, users: [[username, false]], isAvailable: true };
				storage.rooms.push(room);
				socket.emit(notifications.JOIN_ROOM, room);
				io.emit(notifications.ADD_ROOM, room);
			}
		});
		
		socket.on(notifications.JOIN_ROOM, roomName => {
			socket.join(roomName);
			storage.activeUsers.set(username, { room: roomName });
			const rooms = storage.rooms;
			const searchRoom = rooms.find(el => el.name === roomName);
			const searchRoomIdx = rooms.indexOf(searchRoom);
			const updateRoom = { ...searchRoom };
			const newUser = [username, false];
			updateRoom.users.push(newUser);
			const numberOfUsers = updateRoom.users.length;
			if (numberOfUsers === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
				io.emit(notifications.REMOVE_ROOM, roomName);
				updateRoom.isAvailable = false;
			}
			
			rooms[searchRoomIdx] = updateRoom;
			storage.rooms = rooms;
			socket.emit(notifications.JOIN_ROOM, updateRoom);
			socket.broadcast.to(roomName).emit(notifications.ADD_USER_TO_ROOM, newUser);
			io.emit(notifications.UPDATE_NUMBER_OF_USERS, ({ name: roomName, numberOfUsers }));
		})

		socket.on(notifications.LEAVE_ROOM, () => {
			const roomName = helpers.getRoomName(username);
			socket.leave(roomName);
			storage.activeUsers.set(username, { room: null });
			const rooms = storage.rooms;
			const searchRoom = rooms.find(el => el.name === roomName);
			const updateUsers = searchRoom.users;
			remove(updateUsers, (el) => el[0] === username);
			if (!updateUsers.length) {
				remove(rooms, (n) => n.name === roomName);
				storage.rooms = rooms;
				io.emit(notifications.REMOVE_ROOM, roomName);
			} else {
				socket.broadcast.to(roomName).emit(notifications.LEAVE_ROOM, username);
				const roomIdx = rooms.indexOf(searchRoom);
				const isEveryUserReady = updateUsers.every(el => el[1]);
				if (isEveryUserReady) {
					helpers.prepareForGame({
						io,
						searchRoom,
						roomName,
						rooms,
						updateUsers,
						roomIdx
					})
				} else {
					const updateRoom = { ...searchRoom, users: updateUsers };
					if (!searchRoom.isAvailable) {
						io.emit(notifications.ADD_ROOM, updateRoom);
						searchRoom.isAvailable = true;
					}
					io.emit(notifications.UPDATE_NUMBER_OF_USERS, ({ name: roomName, numberOfUsers: updateRoom.users.length }));
					rooms[roomIdx] = updateRoom;
					storage.rooms = rooms;
				}
			}
		});
		
		socket.on(notifications.CHANGE_STATUS, () => {
			const roomName = helpers.getRoomName(username);
			const rooms = storage.rooms;
			let searchRoom = rooms.find(el => el.name === roomName);
			const searchRoomIdx = storage.rooms.indexOf(searchRoom);
			const updateUsers = searchRoom.users;
			const searchUser = updateUsers.find(el => el[0] === username);
			const searchUserIdx = searchRoom.users.indexOf(searchUser);
			const updateUser = [searchUser[0], !searchUser[1]];
			updateUsers[searchUserIdx] = updateUser;
			const isEveryUserReady = updateUsers.every(el => el[1]);
			if (isEveryUserReady) {
				helpers.prepareForGame({
					io,
					searchRoom,
					roomName,
					rooms,
					updateUsers,
					roomIdx: searchRoomIdx
				})
			} else {
				searchRoom = { ...searchRoom, users: updateUsers };
				rooms[searchRoomIdx] = searchRoom;
				storage.rooms = rooms;
			}
			socket.broadcast.to(roomName).emit(notifications.CHANGE_STATUS, updateUser);
		});
		
		socket.on(notifications.GET_GAME_CONFIG, () => {
			const gameConfig = helpers.setGameConfig({ storage, username });
			socket.emit(notifications.SET_GAME_CONFIG, gameConfig);
		});
		
		socket.on(notifications.SET_PROGRESS, progress => {
			const roomName = helpers.getRoomName(username);
			const game = storage.games.get(roomName);
			game.users[username].progress = progress;
			if (progress === 100) {
				game.users[username].finishTime = new Date();
			}
			storage.games.set(roomName, game);
			io.to(roomName).emit(notifications.UPDATE_PROGRESS, ({ username, progress }));
		});
		
		socket.on(notifications.GET_RESULT, () => {
			const roomName = helpers.getRoomName(username);
			const users = storage.games.get(roomName).users;
			const keys = Object.keys(users);
			const usersArray = keys.map(el => {
				return {
					username: el,
					progress: users[el].progress,
					finishTime: users[el].finishTime
				}
			});
			const finishByTime = [...usersArray].filter(el => el.finishTime > 0).sort((a, b) => a.finishTime - b.finishTime);
			const finishByProgress = [...usersArray].filter(el => el.finishTime === 0).sort((a, b) => b.progress - a.progress);
			const result = [...finishByTime, ...finishByProgress].map(el => el.username);
			socket.emit(notifications.SET_RESULT, result);
			const rooms = storage.rooms;
			const searchRoom = rooms.find(el => el.name === roomName);
			const updateUsers = searchRoom.users.map(el => [el[0]], false);
			const updateRoom = { ...searchRoom, users: updateUsers }
			if (updateRoom.users.length < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
				updateRoom.isAvailable = true;
				io.emit(notifications.ADD_ROOM, updateRoom)
			}
			const searchRoomIdx = storage.rooms.indexOf(searchRoom);
			rooms[searchRoomIdx] = updateRoom;
			storage.rooms = rooms;
			io.to(roomName).emit(notifications.CHANGE_STATUS, [username, false]);
			io.to(roomName).emit(notifications.UPDATE_PROGRESS, ({ username, progress: 0 }));
		})
		
		socket.on(notifications.DISCONNECT, () => {
			const roomName = helpers.getRoomName(username);
			if (roomName) {
				socket.broadcast.to(roomName).emit(notifications.LEAVE_ROOM, username);
				const game = storage.games.get(roomName);
				if (game) {
					delete game.users[username];
					storage.games.set(roomName, game)
				}
				
				const rooms = storage.rooms;
				const searchRoom = rooms.find(el => el.name === roomName);
				const updateUsers = searchRoom.users;
				remove(updateUsers, (el) => el[0] === username);
				if (!updateUsers.length) {
					remove(rooms, (n) => n.name === roomName);
					storage.rooms = rooms;
					io.emit(notifications.REMOVE_ROOM, roomName);
				} else {
					socket.broadcast.to(roomName).emit(notifications.LEAVE_ROOM, username);
					const roomIdx = rooms.indexOf(searchRoom);
					const updateRoom = {...searchRoom, users: updateUsers}
					rooms[roomIdx] = updateRoom;
					storage.rooms = rooms;
					io.emit(notifications.UPDATE_NUMBER_OF_USERS, ({ name: roomName, numberOfUsers: updateRoom.users.length }));
				}
			}
			
			storage.activeUsers.delete(username);
		});
	});
};
