export const texts = [
  "How many programmers does it take to change a light bulb? None – It’s a hardware problem",
  "There are only 10 kinds of people in this world: those who know binary and those who don’t.",
  "The generation of random numbers is too important to be left to chance.",
  "I'm thinking about studying Programming next year. So i can C# in 2023",
  "Real Programmers always count from 0.",
  "Why Programmers like dark mode? Because light attracts bugs ",
  "What is a Programmer favourite hangout place? Foo Bar"
];

export default { texts };
