import { showInputModal } from '../views/modal.mjs';
import { changeReadyStatus } from '../views/user.mjs';

function onCreateRoom(addRoomEvent) {
  let roomName;
  const title = 'Room name';
  const onChange = (value) => roomName = value
  const onSubmit = () => addRoomEvent(roomName)
  showInputModal({ title, onChange, onSubmit });
}

function onBackToRooms(backToRoomsEvent) {
  const usersWrapper = document.getElementById('users-wrapper');
  usersWrapper.innerHTML = '';
  const gamePage = document.getElementById('game-page');
  gamePage.classList.add('display-none');
  const roomPage = document.getElementById('rooms-page');
  roomPage.classList.remove('display-none');
  backToRoomsEvent();
}

function onReady ({e, username, readyEvent }) {
  let ready;
  if (e.target.innerHTML === 'READY') {
    e.target.innerHTML = 'NOT READY';
    ready = true
  } else {
    e.target.innerHTML = 'READY';
    ready = false
  }
  changeReadyStatus({ username, ready });
  readyEvent(ready);
}

export { onCreateRoom, onBackToRooms, onReady }