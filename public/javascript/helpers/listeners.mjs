import { showMessageModal, showResultsModal } from '../views/modal.mjs';
import { appendRoomElement, removeRoomElement, updateNumberOfUsersInRoom } from '../views/room.mjs';
import { appendUserElement, removeUserElement, changeReadyStatus, setProgress } from '../views/user.mjs';

function loginError() {
  const message = 'There is an active user with such name. \n Please chose another name.';
  const onClose = () => {
    sessionStorage.clear();
    window.location.replace("/login");
  }
  
  showMessageModal({ message, onClose });
}

function roomNameError() {
  const message = 'There is a room with such name. \n Please, give your room another name.';
  showMessageModal({ message });
}

function renderRooms(data, joinEvent) {
 data.forEach(({ name, users }) => {
   const numberOfUsers = users.length;
   const onJoin = () => joinEvent(name);
   appendRoomElement({ name, numberOfUsers, onJoin });
 });
}

function addRoom({ name, users }, joinEvent) {
  const numberOfUsers = users.length;
  const onJoin = () => joinEvent(name);
  appendRoomElement({ name, numberOfUsers, onJoin })
}

function joinRoom(data, currentUser) {
  const { name, users } = data;
  const roomPage = document.getElementById('rooms-page');
  roomPage.classList.add('display-none');
  const gamePage = document.getElementById('game-page');
  gamePage.classList.remove('display-none');
  const roomName = document.getElementById('room-name');
  roomName.innerHTML = name;
  users.forEach(user => {
    const isCurrentUser = user[0] === currentUser;
    appendUserElement({ username: user[0], ready: user[1], isCurrentUser })
  })
}

function addUserToRoom(data, currentUser) {
  const [ username, ready ] = data;
  const isCurrentUser = username === currentUser;
  appendUserElement({ username, ready, isCurrentUser });
}

function removeRoom(data) {
  removeRoomElement(data);
}

function leaveRoom(username) {
  removeUserElement(username);
}

function updateNumberOfUsers(data) {
  updateNumberOfUsersInRoom({...data})
}

function changeStatus([ username, ready ]) {
  changeReadyStatus({ username, ready });
}

function prepareForGame({ time, getGameConfigEvent, gameEvent }) {
  const quitRoomBtn = document.getElementById('quit-room-btn');
  const readyBtn = document.getElementById('ready-btn');
  const timer = document.getElementById('timer');
  quitRoomBtn.classList.add('display-none');
  readyBtn.classList.add('display-none');
  timer.classList.remove('display-none');
  timer.innerHTML = '';
  getGameConfigEvent();
  let counterForPreparing = setTimeout(function tick() {
    const value = time;
    timer.innerHTML = `${value}`;
    time = value - 1;
    if (time < 0) {
      clearInterval(counterForPreparing);
      timer.classList.add('display-none')
      gameEvent();
      return;
    }
    counterForPreparing = setTimeout(tick, 1000)
  }, 1000)
}

function setGameConfig(data, gameConfig) {
  const { id, time } = data;
  fetchText(id)
  .then(res => {
    gameConfig.text = res;
    gameConfig.secondsForGame = time;
  })
}

async function fetchText(id) {
  const res = await fetch(`/game/texts/${id}`);
  if(!res.ok) {
    throw new Error(`Could not fetch /game/texts/${id}, received ${res.status}`);
  }
  return res.json();
}

function startGame({ gameConfig, setProgressEvent, getResultEvent }) {
  const { text, secondsForGame } = gameConfig;
  const textContainer = document.getElementById('text-container');
  textContainer.classList.remove('display-none');
  textContainer.innerHTML = '';
  const content = text.split('').map(el => {
    const wrapper = document.createElement('span');
    wrapper.innerHTML = el;
    return wrapper;
  });
  content[0].classList.add('first-letter');
  textContainer.append(...content);
  
  let count = 0;
  
  const keyHandler = function(e) {
    if (content[count].innerHTML === e.key) {
      content[count].classList.add('done');
      const progress = Math.ceil(100 * (count + 1) / text.length);
      setProgressEvent(progress);
      if (progress === 100) {
        window.removeEventListener('keydown', keyHandler);
      }
      return count++;
    }
  }
  
  window.addEventListener('keydown', keyHandler);
  
  const gameTimer = document.getElementById('game-timer');
  gameTimer.classList.remove('display-none');
  const gameTimerSeconds = document.getElementById('game-timer-seconds');
  gameTimerSeconds.innerHTML = secondsForGame;
  let time = secondsForGame - 1;
  let counterForGame = setTimeout(function tick() {
    const value = time;
    gameTimerSeconds.innerHTML = `${value}`;
    time = value - 1;
    if (time < 0) {
      window.removeEventListener('keydown', keyHandler);
      getResultEvent();
      clearInterval(counterForGame);
      return;
    }
    counterForGame = setTimeout(tick, 1000)
  }, 1000);
}

function updateProgress(data) {
  setProgress({...data});
}

function setResult({ data, onClose }) {
  showResultsModal({ usersSortedArray: data, onClose });
  const gameTimer = document.getElementById('game-timer');
  gameTimer.classList.add('display-none');
}

function onResultClose() {
  const textContainer = document.getElementById('text-container');
  const readyBtn = document.getElementById('ready-btn');
  const quitRoomBtn = document.getElementById('quit-room-btn');
  textContainer.classList.add('display-none');
  readyBtn.classList.remove('display-none');
  readyBtn.innerHTML = 'READY';
  quitRoomBtn.classList.remove('display-none');
}

export {
  loginError,
  roomNameError,
  renderRooms,
  addRoom,
  joinRoom,
  removeRoom,
  addUserToRoom,
  leaveRoom,
  updateNumberOfUsers,
  changeStatus,
  prepareForGame,
  setGameConfig,
  startGame,
  updateProgress,
  setResult,
  onResultClose
};