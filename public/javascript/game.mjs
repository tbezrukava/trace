import notifications from './helpers/notifications.mjs';
import * as listeners from './helpers/listeners.mjs';
import * as handlers from './helpers/handlers.js';

const username = sessionStorage.getItem('username');

if (!username) {
	window.location.replace('/login');
}

const socket = io('', { query: { username } });

const gameConfig = {
	text: null,
	secondsForGame: null
}

const addRoomBtn = document.getElementById('add-room-btn');
const quitRoomBtn = document.getElementById('quit-room-btn');
const readyBtn = document.getElementById('ready-btn');
addRoomBtn.addEventListener('click', () => handlers.onCreateRoom(createRoomEvent));
quitRoomBtn.addEventListener('click', () => handlers.onBackToRooms(backToRoomsEvent));
readyBtn.addEventListener('click', (e) => handlers.onReady({ e, username, readyEvent }));


const createRoomEvent = data => socket.emit(notifications.CREATE_ROOM, data);
const backToRoomsEvent = () => socket.emit(notifications.LEAVE_ROOM);
const readyEvent = () => socket.emit(notifications.CHANGE_STATUS);
const joinEvent = data => socket.emit(notifications.JOIN_ROOM, data);
const getGameConfigEvent = () => socket.emit(notifications.GET_GAME_CONFIG);
const setProgressEvent = data => socket.emit(notifications.SET_PROGRESS, data);
const getResultEvent = () => socket.emit(notifications.GET_RESULT);
const gameEvent = () => listeners.startGame({ gameConfig, setProgressEvent, getResultEvent });


socket.on(notifications.LOGIN_ERROR, listeners.loginError);
socket.on(notifications.ROOM_NAME_ERROR, listeners.roomNameError);
socket.on(notifications.RENDER_ROOMS, data => listeners.renderRooms(data, joinEvent));
socket.on(notifications.ADD_ROOM, data => listeners.addRoom(data, joinEvent));
socket.on(notifications.JOIN_ROOM, data => listeners.joinRoom(data, username));
socket.on(notifications.REMOVE_ROOM, data => listeners.removeRoom(data));
socket.on(notifications.ADD_USER_TO_ROOM, data => listeners.addUserToRoom(data, username));
socket.on(notifications.LEAVE_ROOM, data => listeners.leaveRoom(data));
socket.on(notifications.UPDATE_NUMBER_OF_USERS, data => listeners.updateNumberOfUsers(data));
socket.on(notifications.CHANGE_STATUS, data => listeners.changeStatus(data));
socket.on(notifications.PREPARE_FOR_GAME, data => listeners.prepareForGame({ time: data, getGameConfigEvent, gameEvent }));
socket.on(notifications.SET_GAME_CONFIG, data => listeners.setGameConfig(data, gameConfig));
socket.on(notifications.UPDATE_PROGRESS, data => listeners.updateProgress(data));
socket.on(notifications.SET_RESULT, data => listeners.setResult({ data, onClose: listeners.onResultClose }));